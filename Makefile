REVISION := $(shell git describe)
REVISION += unknown
REVISION := $(word 1, $(REVISION))

BINARIES := $(wildcard cmd/*)
BINARIES_LINUX := $(addsuffix -linux,${BINARIES})
BINARIES_RASP := $(addsuffix -rasp,${BINARIES})
BINARIES_DARWIN := $(addsuffix -darwin,${BINARIES})

.PHONY: release
release: ${BINARIES}

.PHONY: all
all: ${BINARIES_LINUX} ${BINARIES_RASP} ${BINARIES_DARWIN}

.PHONY: ${BINARIES}
${BINARIES}:
	go get ./...
	cd cmd/$(@:cmd/%=%) ; go build -ldflags "-X tools.GitRevision=$(REVISION)" -o ../../$(@:cmd/%=%) ; cd ../..

.PHONY: linux
linux: ${BINARIES_LINUX}

.PHONY: ${BINARIES_LINUX}
${BINARIES_LINUX}:
	mkdir -p release
	cd cmd/$(@:cmd/%-linux=%) ; GOOS=linux GOARCH=amd64 go build -ldflags "-X tools.GitRevision=$(REVISION)" -o ../../release/$(@:cmd/%-linux=%)-$(REVISION)-linux ; cd ../..

.PHONY: rasp
rasp: ${BINARIES_RASP}

.PHONY: ${BINARIES_RASP}
${BINARIES_RASP}:
	mkdir -p release
	cd cmd/$(@:cmd/%-rasp=%) ; GOOS=linux GOARCH=arm64 go build -ldflags "-X tools.GitRevision=$(REVISION)" -o ../../release/$(@:cmd/%-rasp=%)-$(REVISION)-rasp ; cd ../..

.PHONY: darwin
darwin: ${BINARIES_DARWIN}

.PHONY: ${BINARIES_DARWIN}
${BINARIES_DARWIN}:
	mkdir -p release
	cd cmd/$(@:cmd/%-darwin=%)  ; GOOS=darwin GOARCH=amd64 go build -ldflags "-X tools.GitRevision=$(REVISION)" -o ../../release/$(@:cmd/%-darwin=%)-$(REVISION)-darwin ; cd ../..

.PHONY: clean
clean:
	rm -rf release/*
