package main

import (
	"log"
	"os"

	"gitlab.com/sbca/lnd-tools/tools"
)

func main() {
	app := tools.GetApp()
	app.Name = "lnd-recurring"
	app.Usage = "Utility to handle recurring payments"
	app.Action = tools.RecurringCommand

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
