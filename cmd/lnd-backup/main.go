package main

import (
	"log"
	"os"

	"gitlab.com/sbca/lnd-tools/tools"
)

func main() {
	app := tools.GetApp()
	app.Name = "lnd-backup"
	app.Usage = "Utility to handle channel backups"
	app.Action = tools.BackupCommand

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
