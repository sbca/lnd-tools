package main

import (
	"log"
	"os"

	"gitlab.com/sbca/lnd-tools/tools"
)

func main() {
	app := tools.GetApp()
	app.Name = "lnd-acceptor"
	app.Usage = "Utility to take care of channel acceptance"
	app.Flags = append(app.Flags, tools.GetAcceptorFlags()...)
	app.Action = tools.AcceptorCommand

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
