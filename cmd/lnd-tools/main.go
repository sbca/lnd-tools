package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/sbca/lnd-tools/tools"
)

func main() {
	app := tools.GetApp()
	app.Name = "lnd-tools"
	app.Usage = "Collection of tools for LND (lightning deamon)"
	app.Commands = []*cli.Command{
		&cli.Command{
			Name:   "backup",
			Action: tools.BackupCommand,
		},
		&cli.Command{
			Name:   "acceptor",
			Action: tools.AcceptorCommand,
			Flags:  tools.GetAcceptorFlags(),
		},
		&cli.Command{
			Name:   "recurring",
			Action: tools.RecurringCommand,
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}

}
