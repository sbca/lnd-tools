# LND tools

[![tippin.me](https://badgen.net/badge/%E2%9A%A1%EF%B8%8Ftippin.me/@fiksn/F0918E)](https://tippin.me/@fiksn)

This repository contains various (command-line) tools that can be used with [LND (Lightning Daemon)](https://github.com/lightningnetwork/lnd) maintained by
enthusiasts from the [Slovenian Blockchain Association](https://www.sbca.si/). Everybody is welcome to contribute.

## lnd-tools

This program is an amalgamation of all the following tools into one utility. Backup subcomand behaves exactly like lnd-backup and so on.
Sometimes it is easier to carry around just one tool. However due to the different nature of the tools usually on one machine you just need one tool (e.g. lnd-backup on server
performing backups).

## lnd-backup

LND backup remotely backs-up through gRPC every time the static channel backup changes (when you open or close channels).
The backups are encrypted with your seed phrase. This means that you can run this on a remote machine (as long gRPC port is reachable)
and "pull" backups to it only when necessary (gRPC connection however must be open all the time).

This is easier than having some script that sleeps on ```inotifywait``` and "pushes" the backup file when it changes.

Minimal macaroon permissions:
```
lncli bakemacaroon offchain:read info:read
```

(you could also run without info:read but then backup file name will not contain node public key)

## lnd-acceptor

LND acceptor is used to configure what kind of channels to automatically accept. It uses ChanAcceptor gRPC call.

Flags:
```
--minsat value           Minimum channel size in satoshis (default: 0)
--maxsat value           Maximum channel size in satoshis (default: 9223372036854775807)
--minpush value          Minimal amount to push in satoshis or percentage of channel size
--allow value            A hex string of pubkey to unconditionally allow opening channels from (there can be multiple)
--deny value             A hex string of pubkey to unconditionally disallow opening channels from (there can be multiple)
```

With ```-minsat``` you can simulate ```--minchansize``` LND flag without having to restart LND.

Minimal macaroon permissions:
```
lncli bakemacaroon onchain:write offchain:write
```

It fails openly (thus when lnd-acceptor is not running everything is accepted).

*To avoid any misunderstandings: static channel backups can be used to force close the channel and get your money back.
Copying the whole state is dangerous as you might accidentally push out and old state and lose all funds*

### LND fork

Notice that we are using: https://github.com/Jeiwan/lnd/tree/channelacceptor_rejection_reason
until https://github.com/lightningnetwork/lnd/pull/4202 gets merged-in. The rename is done through [go.mod](./go.mod).
Else it is not possible to specify rejection code.

## lnd-recurring

LND recurring payments allows automatic payments of recurring invoices (same destination) below a given threshold (per day/week/month/year) defined in configuration file.
