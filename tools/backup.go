package tools

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"

	"github.com/lightningnetwork/lnd/lnrpc"

	"github.com/urfave/cli/v2"
	"gitlab.com/sbca/lnd-tools/ext"
)

// Command
func BackupCommand(ctx *cli.Context) error {
	return Command(ctx, Backup)
}

// Backup - main entrypoint
func Backup(tick *Tick, ctx *cli.Context) error {
	var name string

	if tick.executing {
		return nil
	}

	tick.executing = true
	defer func() {
		tick.executing = false
	}()

	fmt.Printf("[%v] Started...\n", time.Now().Format(FMT))

	client, cleanUp := ext.GetClient(ctx)
	defer cleanUp()

	info, err := client.GetInfo(context.Background(), &lnrpc.GetInfoRequest{})
	if err == nil {
		fmt.Printf("[%v] Connected to %v\n", time.Now().Format(FMT), info.IdentityPubkey)
		name = fmt.Sprintf("%v.backup", info.IdentityPubkey)
	} else {
		rand, err := ext.GenerateRandomString(16)
		if err != nil {
			return err
		}

		fmt.Printf("[%v] Connected to anonymous node\n", time.Now().Format(FMT))
		name = fmt.Sprintf("%v.backup", rand)
	}

	err = initialBackup(name, client)
	if err != nil {
		return err
	}

	err = periodicBackup(name, client)
	if err != nil {
		return err
	}

	return nil
}

func verifyNewBackup(name string, content []byte, client lnrpc.LightningClient) error {

	fmt.Printf("[%v] New backup created - %v\n", time.Now().Format(FMT), name)

	rand, err := ext.GenerateRandomString(16)
	if err != nil {
		return err
	}

	tempName := fmt.Sprintf("temp-%v-%v", rand, name)

	err = ioutil.WriteFile(tempName, content, 0644)

	// Make a round-trip to check file is safe
	contents, err := ioutil.ReadFile(tempName)
	_, err = client.VerifyChanBackup(context.Background(), &lnrpc.ChanBackupSnapshot{MultiChanBackup: &lnrpc.MultiChanBackup{MultiChanBackup: contents}})
	if err != nil {
		return err
	}

	// Atomic rename if we came this far
	err = os.Rename(tempName, name)
	if err != nil {
		return err
	}

	fmt.Printf("[%v] New backup verified - %v\n", time.Now().Format(FMT), name)

	return nil
}

func periodicBackup(name string, client lnrpc.LightningClient) error {
	fmt.Printf("[%v] Subscribing for new backups\n", time.Now().Format(FMT))
	stream, err := client.SubscribeChannelBackups(context.Background(), &lnrpc.ChannelBackupSubscription{})
	if err != nil {
		return err
	}
	fmt.Printf("[%v] Subscribed for new backups\n", time.Now().Format(FMT))

	for {
		backup, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		err = verifyNewBackup(name, backup.MultiChanBackup.MultiChanBackup, client)
		if err != nil {
			return err
		}
	}

	return nil
}

func initialBackup(name string, client lnrpc.LightningClient) error {
	resp, err := client.ExportAllChannelBackups(context.Background(), &lnrpc.ChanBackupExportRequest{})
	if err != nil {
		return err
	}

	err = verifyNewBackup(name, resp.MultiChanBackup.MultiChanBackup, client)

	if err != nil {
		return err
	}

	return nil
}
