package tools

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/sbca/lnd-tools/ext"
)

// Format string for all date operations
const FMT = "2006-01-02 15:04:05.000000"

type Handler func(*Tick, *cli.Context) error

type Tick struct {
	ticker    *time.Ticker
	executing bool
}

var (
	GitRevision = "unknown"
)

func GetApp() *cli.App {
	app := cli.NewApp()
	app.Version = GitRevision
	app.Flags = ext.GetLndFlags()
	return app
}

func Command(ctx *cli.Context, handler Handler) error {
	fmt.Printf("[%v] Starting...\n", time.Now().Format(FMT))

	tick := &Tick{
		ticker: time.NewTicker(30 * time.Second),
	}
	defer tick.ticker.Stop()

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	for {
		go handler(tick, ctx)

		select {
		case <-tick.ticker.C:
			go handler(tick, ctx)
		case <-sigc:
			fmt.Printf("[%v] Terminating...\n", time.Now().Format(FMT))
			return nil
		}
	}
	return nil
}
