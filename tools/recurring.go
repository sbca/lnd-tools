package tools

import (
	"context"
	"fmt"
	"time"

	"github.com/lightningnetwork/lnd/lnrpc"

	"github.com/urfave/cli/v2"
	"gitlab.com/sbca/lnd-tools/ext"
)

// Command
func RecurringCommand(ctx *cli.Context) error {
	return Command(ctx, Recurring)
}

// Recurring - main entrypoint
func Recurring(tick *Tick, ctx *cli.Context) error {
	if tick.executing {
		return nil
	}

	tick.executing = true
	defer func() {
		tick.executing = false
	}()

	fmt.Printf("[%v] Started...\n", time.Now().Format(FMT))

	client, cleanUp := ext.GetClient(ctx)
	defer cleanUp()

	lastIndex := int64(-1)
	maxList := uint64(100)
	invoices := make([]*lnrpc.Invoice, 500)

	for {
		list, err := client.ListInvoices(context.Background(), &lnrpc.ListInvoiceRequest{PendingOnly: false, IndexOffset: uint64(lastIndex + 1), NumMaxInvoices: maxList})
		if err != nil {
			return err
		}

		if len(list.Invoices) == 0 {
			break
		}

		lastIndex = int64(list.LastIndexOffset)
		invoices = append(invoices, list.Invoices...)
	}

	fmt.Println(len(invoices))

	// CreationDate
	//DecodePayReq(ctx context.Context, in *PayReqString, opts ...grpc.CallOption) (*PayReq, error)

	//client.InvoiceSubscription(context.Background())

	/*
		stream, err := client.InvoiceSubscription(context.Background())
		if err != nil {
			return err
		}
		fmt.Printf("[%v] Subscribed to channel acceptor\n", time.Now().Format(FMT))

		for {
			req, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				return err
			}

			fmt.Printf("[%v] Node %v is trying to establish a %v sat (pushed %v sat) channel\n", time.Now().Format(FMT), hex.Dump(req.NodePubkey), req.FundingAmt, req.PushAmt)
			resp := canEstablish(req, ctx)

			stream.Send(resp)
		}
	*/

	return nil
}
