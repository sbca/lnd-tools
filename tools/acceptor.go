package tools

import (
	"context"
	"encoding/hex"
	"fmt"
	"io"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/lightningnetwork/lnd/lnrpc"
	"gitlab.com/sbca/lnd-tools/ext"

	"github.com/urfave/cli/v2"
)

// Command
func AcceptorCommand(ctx *cli.Context) error {
	return Command(ctx, Acceptor)
}

// Acceptor - main entrypoint
func Acceptor(tick *Tick, ctx *cli.Context) error {
	if tick.executing {
		return nil
	}

	tick.executing = true
	defer func() {
		tick.executing = false
	}()

	fmt.Printf("[%v] Started...\n", time.Now().Format(FMT))

	client, cleanUp := ext.GetClient(ctx)
	defer cleanUp()

	stream, err := client.ChannelAcceptor(context.Background())
	if err != nil {
		return err
	}
	fmt.Printf("[%v] Subscribed to channel acceptor\n", time.Now().Format(FMT))

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		fmt.Printf("[%v] Node %v is trying to establish a %v sat (pushed %v sat) channel\n", time.Now().Format(FMT), hex.Dump(req.NodePubkey), req.FundingAmt, req.PushAmt)
		resp := canEstablish(req, ctx)

		stream.Send(resp)
	}

	return nil
}

func reject(msg, remote string) *lnrpc.ChannelAcceptResponse {
	resp := &lnrpc.ChannelAcceptResponse{
		Accept:          false,
		RejectionReason: msg,
	}

	fmt.Printf("[%v] Rejected channel from %v\n", time.Now().Format(FMT), remote)

	return resp
}

func canEstablish(req *lnrpc.ChannelAcceptRequest, ctx *cli.Context) *lnrpc.ChannelAcceptResponse {
	resp := &lnrpc.ChannelAcceptResponse{
		Accept:          true,
		RejectionReason: "all good",
	}

	if isInList("allow", hex.Dump(req.NodePubkey), ctx) {
		fmt.Printf("[%v] Allowing %v - since it is on allowlist\n", time.Now().Format(FMT), hex.Dump(req.NodePubkey))
		return resp
	}

	if isInList("deny", hex.Dump(req.NodePubkey), ctx) {
		fmt.Printf("[%v] Denying %v - since it is on denylist\n", time.Now().Format(FMT), hex.Dump(req.NodePubkey))
		return reject("Not allowed", hex.Dump(req.NodePubkey))
	}

	minSat := uint64(ctx.Int64("minsat"))

	if req.FundingAmt < minSat {
		return reject(fmt.Sprintf("Funding amount should at least %v (you tried %v)", ctx.Int64("minsat"), req.FundingAmt), hex.Dump(req.NodePubkey))
	}

	if req.FundingAmt > uint64(ctx.Int64("maxsat")) {
		return reject(fmt.Sprintf("Funding amount should at most %v (you tried %v)", ctx.Int64("maxsat"), req.FundingAmt), hex.Dump(req.NodePubkey))
	}

	minPushInt, err := calcMinPush(ctx)
	if err != nil {
		return resp
	}

	if req.PushAmt < minPushInt {
		return reject(fmt.Sprintf("Please push at least %v (%v) (you tried %v)", minPushInt, ctx.String("minpush"), req.PushAmt), hex.Dump(req.NodePubkey))
	}

	return resp
}

func calcMinPush(ctx *cli.Context) (uint64, error) {
	minSat := uint64(ctx.Int64("minsat"))

	minPush := ctx.String("minpush")
	minPushInt := uint64(0)

	if strings.Contains(minPush, "%") {
		temp, err := strconv.ParseInt(strings.ReplaceAll(minPush, "%", ""), 0, 64)
		if err != nil {
			return minPushInt, err
		}

		minPushInt = uint64(math.Round(float64(temp) * float64(minSat) * 0.01))
	} else {
		temp, err := strconv.ParseInt(minPush, 0, 64)
		if err != nil {
			return minPushInt, err
		}

		minPushInt = uint64(temp)
	}

	return minPushInt, nil
}

func isInList(listName, nodeName string, ctx *cli.Context) bool {
	list := ctx.StringSlice(listName)
	for _, one := range list {
		if one == "" {
			continue
		}

		if strings.ToUpper(one) == strings.ToUpper(nodeName) {
			return true
		}
	}

	return false
}

// GetAcceptorFlags - All flags that are accepted
func GetAcceptorFlags() []cli.Flag {
	newFlags := []cli.Flag{
		&cli.Int64Flag{
			Name:  "minsat",
			Value: 0,
			Usage: "Minimum channel size in satoshis",
		},
		&cli.Int64Flag{
			Name:  "maxsat",
			Value: math.MaxInt64,
			Usage: "Maximum channel size in satoshis",
		},
		&cli.StringFlag{
			Name:  "minpush",
			Value: "",
			Usage: "Minimal amount to push in satoshis or percentage of channel size",
		},
		&cli.StringSliceFlag{
			Name:  "allow",
			Value: cli.NewStringSlice(""),
			Usage: "A hex string of pubkey to unconditionally allow opening channels from (there can be multiple)",
		},

		&cli.StringSliceFlag{
			Name:  "deny",
			Value: cli.NewStringSlice(""),
			Usage: "A hex string of pubkey to unconditionally disallow opening channels from (there can be multiple)",
		},
	}
	return newFlags
}
