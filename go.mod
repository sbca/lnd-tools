module gitlab.com/sbca/lnd-tools

go 1.13

require (
	github.com/btcsuite/btcutil v1.0.2
	github.com/lightningnetwork/lnd v0.0.0-00010101000000-000000000000
	github.com/urfave/cli v1.18.0
	github.com/urfave/cli/v2 v2.2.0
	google.golang.org/grpc v1.31.1
	gopkg.in/macaroon.v2 v2.1.0
)

replace github.com/lightningnetwork/lnd => github.com/Jeiwan/lnd v0.10.0-beta.rc2.0.20200614014313-1b0ac21a21eb
